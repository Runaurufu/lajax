/**
 *  Author: Damian Pawlik
 *  Website: http://runaurufu.com
 *  Contact: runaurufu[at]gmail.com
 *  This script can be used free of charge but cannot be modified without author knowledge and his agreement.
 *  This script is provided as is without any warranty.
 */

$$Semaphore = new Object();
$$Semaphore.Tasks = new Array();

$$.getFormArgs = function(form$$Node)
{
  var args = "";
  var col = form$$Node.$.elements;
  for(var i=0; i<col.length; i++)
  {
    if(col[i].checked === false && (col[i].type === "checkbox" || col[i].type === "radio" ))
      continue;

    var key;
    if(col[i].name !== undefined)
      key = col[i].name;
    else if(col[i].id !== undefined)
      key = col[i].id;
    else
      continue;
    args+="&"+encodeURIComponent(key)+"="+encodeURIComponent(col[i].value);
  }
  args=args.substr(1);
  return args;
};
$$.getForm = function(childNode)
{
  while(childNode !== null)
  {
    if(childNode.nodeName === "FORM")
      return childNode;
    
    childNode = childNode.parentNode;
  }
  return null;
};
$$.ajax = function(settings)
{
  return new $$Ajax(settings);
};

/**
 * Open specified url with method and parameters in currently browsed page.
 * @param {string} url
 * @param {string} method POST/GET
 * @param {array} params {key: value}
 */
$$.open = function(url, method, params)
{
  var form = document.createElement("form");
  form.setAttribute("method", method);
  form.setAttribute("action", url);

  for(var key in params)
  {
    if(params.hasOwnProperty(key))
    {
      var hiddenField = document.createElement("input");
      hiddenField.setAttribute("type", "hidden");
      hiddenField.setAttribute("name", key);
      hiddenField.setAttribute("value", params[key]);

      form.appendChild(hiddenField);
     }
  }

  document.body.appendChild(form);
  form.submit();
}

/**
 * 
 * @param {type} selector
 * @param {type} core
 * @returns {$$Node|$$NodeList|$$}
 */
function $$(selector, core) //if selector is a node (normal node) than automaticaly turn it to $$Node
{
  if(core === undefined)
    core = document;
  else if(core.$ !== undefined)
    core = core.$;

  if(selector !== undefined)
  {
    if(typeof(selector) === "object")
      return new $$Node(selector);
    
    switch(selector.substr(0, 1))
    {
      case '#':
        return new $$Node(core.getElementById(selector.substr(1)));
      case '.':
        return new $$NodeList(core.getElementsByClassName(selector.substr(1)));
      case '$':
        return new $$NodeList(core.getElementsByName(selector.substr(1)));
      case '@':
        return new $$NodeList(core.getElementsByTagName(selector.substr(1)));
      default:
        return new $$Node(selector);
    }
  }
  
  /**
   * @deprecated Use $$.ajax(settings).send() instead.
   * @param {type} settings
   */
  this.ajax = function(settings)
  {
    $$.ajax(settings).send();
  };

  return this;
}

function $$Node(node)
{
  this.$ = node;

  /**
   * Returns parent node of current node or null if no parent exists.
   * @returns {$$Node}
   */
  this.parentNode = function()
  {
    if (this.$.parentNode !== null)
      return new $$Node(this.$.parentNode);
    else
      return null;
  };

  /**
   * Returns child nodes in newly created node list.
   * @returns {$$NodeList}
   */
  this.childNodes = function()
  {
    var nodes = [];
    for (var i = 0, max = this.$.childNodes.length; i < max; i++)
      nodes.push(this.$.childNodes[i]);
    return new $$NodeList(nodes);
  };
    
  this.addEventCommand = function(eventType, command)
  {
    this.$.setAttribute(eventType, this.$.getAttribute(eventType)+";"+command);
    return this;
  };

  this.show = function()
  {
    var style = this.$.getAttribute('style') === null ? "" : this.$.getAttribute('style');
    this.$.setAttribute('style',style.replace(/display:[ ]?none;/,''));
    return this;
  };

  this.hide = function()
  {
    this.$.style.display = 'none';
    return this;
  };

  this.toggle = function()
  {
    return this.$.style.display === "none" ? this.show() : this.hide();
  };

  this.dynOpacity = function(finalValue, time)
  {
    var startValue = node.style.opacity;
    if(startValue.length === 0)
    {
      startValue = 1.0;
      node.style.opacity = 1.0;
    }

    var diffValue = finalValue - startValue;
    diffValue = Number(Number(diffValue / (time / 40)).toFixed(4));

    var intervalIdent = self.setInterval(function(){node.style.opacity = Number(node.style.opacity) + diffValue;},40);

    window.setTimeout(function(){
      window.clearInterval(intervalIdent);
      node.style.opacity = finalValue;}, time+30);

    return this;
  };
    
  this.dynSize = function(width, height, time)
  {
    var startWidth = this.$.style.width;
    var startHeight = this.$.style.height;
	
    if(startWidth.length === 0)
    {
      startWidth = node.offsetWidth;
      node.style.width = startWidth+"px";
      startWidth -= node.offsetWidth - parseInt( node.style.width);
      node.style.width = startWidth+"px";

      startHeight = node.offsetHeight;
      node.style.height = startHeight+"px";
      startHeight -= node.offsetHeight - parseInt( node.style.height);
      node.style.height = startHeight+"px";
    }

    var diffWidth = width - startWidth;
    diffWidth = Number(Number(diffWidth / (time / 40)).toFixed(4));

    var diffHeight = height - startHeight;
    diffHeight = Number(Number(diffHeight / (time / 40)).toFixed(4));

    var intervalIdent = self.setInterval(function(){
      node.style.width = parseInt(node.style.width)+diffWidth+"px";
      node.style.height = parseInt(node.style.height)+diffHeight+"px";
    },40);

    window.setTimeout(function(){
	    window.clearInterval(intervalIdent);
	    node.style.width = width;node.style.height = height;
    }, time+30);
	
    return this;
  };

  this.load = function(url, method, postData)
  {
    var me = this;
    if(method === undefined)
      method = "GET";
    
    var ajax;
    if(method === "POST" || method === "post")
      ajax = $$.ajax({method:method, data:postData, url:url, success:function(resp){me.fill(resp.responseText);}});
    else
      ajax = $$.ajax({method:method, url:url, success:function(resp){me.fill(resp.responseText);}});
    ajax.send();

    return this;
  };
  
  this.loadForm = function(form$$Node, url, method)
  {
    if(method === undefined)
    {
      if(form$$Node.$.method === undefined)
        method = "POST";
      else
        method = form$$Node.$.method;
    }

    if(url === undefined)
    {
      if(form$$Node.$.action === undefined)
        url = "";
      else
        url = form$$Node.$.action;
    }

    var args = $$.getFormArgs(form$$Node);
    var me = this;
    var ajax;
    if(method === "POST" || method === "post")
      ajax = $$.ajax({method:method, data:args, url:url, success:function(resp){me.fill(resp.responseText);}});
    else
      ajax = $$.ajax({method:method, url:url+"?"+args, success:function(resp){me.fill(resp.responseText);}});
    ajax.send();
    
    return this;
  };

  this.fill = function(text)
  {
    switch(this.$.type)
    {
      case 'text':
      case 'hidden':
      case 'password':
      case 'button':
      case 'textarea':
        this.$.value = text;
        break;
      default:
        this.$.innerHTML = text;
        break;
    }
    return this;
  };

  this.value = function(text)
  {
    if(text === undefined)
    {
      switch(this.$.type)
      {
        case 'text':
        case 'hidden':
        case 'password':
        case 'button':
        case 'textarea':
        case 'radio':
          return this.$.value;
        default:
          return this.$.innerHTML;
      }
    }
    else
    {
      switch(this.$.type)
      {
        case 'text':
        case 'hidden':
        case 'password':
        case 'button':
        case 'textarea':
        case 'radio':
          this.$.value = text;
          break;
        default:
          this.$.innerHTML = text;
          break;
      }
      return this;
    }
  };
  return this;
}

function $$NodeList(nodeList)
{
  this.$ = nodeList;
  this.$Node = [];

  for(var i=0; i<nodeList.length; i++)
  {
    elem = new $$Node(nodeList[i]);
    if(elem !== null)
      this.$Node.push(elem);
  }
  
  /**
   * Returns first Node from the list.
   * @returns {$$Node}
   */
  this.first = function()
  {
    if(this.$Node.length > 0)
      return this.$Node[0];
    else
      return null;
  };
  
  /**
   * Returns last Node from the list.
   * @returns {$$Node}
   */
  this.last = function()
  {
    if(this.$Node.length > 0)
      return this.$Node[this.$Node.length - 1];
    else
      return null;
  };
  
  this.withClassName = function(className)
  {
    var nodes = [];
    for (var i = 0, max = this.$.length; i < max; i++)
    {
      if(this.$[i].className === className)
        nodes.push(this.$[i]);
    }
    return new $$NodeList(nodes);
  };
  
  this.withName = function(name)
  {
    var nodes = [];
    for (var i = 0, max = this.$.length; i < max; i++)
    {
      if(this.$[i].name === name)
        nodes.push(this.$[i]);
    }
    return new $$NodeList(nodes);
  };
  
  this.withTagName = function(tagName)
  {
    var nodes = [];
    for (var i = 0, max = this.$.length; i < max; i++)
    {
      if(String.equalIgnoreCase(this.$[i].tagName, tagName))
        nodes.push(this.$[i]);
    }
    return new $$NodeList(nodes);
  };
  
	this.show = function()
	{
		for(var i=0; i<this.$Node.length; i++)
			this.$Node[i].show();
		return this;
	};
	
	this.hide = function()
	{
		for(var i=0; i<this.$Node.length; i++)
			this.$Node[i].hide();
		return this;
	};
	
  this.addEventCommand = function(eventType, command)
  {
    for(var i=0; i<this.$Node.length; i++)
      this.$Node[i].addEventCommand(eventType, command);
    return this;
  };

  this.dynOpacity = function(finalValue, time)
  {
    for(var i=0; i<this.$Node.length; i++)
      this.$Node[i].dynOpacity(finalValue, time);
    return this;
  };

  this.radio = function(value, activateEvent)
  {
    if(value === undefined)
    {
      for(var i=0; i<this.$Node.length; i++)
      {
        if(this.$Node[i].$.checked)
          return this.$Node[i];
      }
      return null;
    }
    else
    {
      for(var i=0; i<this.$Node.length; i++)
      {
        if(this.$Node[i].$.value === value)
        {
          this.$Node[i].$.checked = true;
          if(activateEvent === true)
          {
            this.$Node[i].$.onchange();
          }
          return this.$Node[i];
        }
      }
      return null;
    }
  };
  return this;
}

function $$XHR(settings)
{
  this.xmlHttp = null;

  this.getXHR = function()
  {
    xmlHttp = null;
    try
    { // Firefox, Opera 8.0+, Safari
      xmlHttp=new XMLHttpRequest();
    }
    catch (e)
    { // Internet Explorer
      try
      {
        xmlHttp=new ActiveXObject("Msxml2.XMLHTTP");
      }
      catch (e)
      {
        xmlHttp=new ActiveXObject("Microsoft.XMLHTTP");
      }
    }
    if(xmlHttp === null)
      alert("Browser does not support HTTP Request");
    return xmlHttp;
  };
  
  this.eventProgress = function(func)
  {
    this.xmlHttp.upload.addEventListener("progress", func, false);
    return this;
  };
  
  this.eventLoad = function(func)
  {
    this.xmlHttp.addEventListener("load", func, false);
    return this;
  };
  
  this.eventError = function(func)
  {
    this.xmlHttp.addEventListener("error", func, false);
    return this;
  };
  
  this.eventAbort = function(func)
  {
    this.xmlHttp.addEventListener("abort", func, false);
    return this;
  };
  
  this.sett = function(s)
  {
    xmlHttp=this.xmlHttp;
    if(s.progress !== undefined) this.EventProgress(s.progress);
    if(s.load !== undefined) this.eventLoad(s.load);
    if(s.error !== undefined) this.eventError(s.error);
    if(s.abort !== undefined) this.eventAbort(s.abort);

    this.open(s.method, s.url, s.async, s.user, s.password);
  };
  
  this.open = function(method, url, async, user, password)
  {
    xmlHttp = this.xmlHttp;
    switch(true)
    {
      case method === undefined:
      case url === undefined:
        throw new Error("$$XHR::method & url args are required");
        break;
      case async === undefined:
        xmlHttp.open(method , url, true);
        break;
      case user === undefined:
        xmlHttp.open(method , url, async);
        break;
      case password === undefined:
        xmlHttp.open(method , url, async, user);
        break;
      default:
        xmlHttp.open(method , url, async, user, password);
    }
    /* if(method == "POST" || method == "post")
    xmlHttp.setRequestHeader("Content-Type", "application/x-www-form-urlencoded");*/
    return this;
  };
  
  this.send = function()
  {
    xmlHttp = this.xmlHttp;
    xmlHttp.setRequestHeader("Cache-Control", "no-cache");
    xmlHttp.send();
  };
  
  this.sendFormData = function(formData)
  {
    xmlHttp = this.xmlHttp;
    xmlHttp.setRequestHeader("Cache-Control", "no-cache");
    xmlHttp.send(formData);
  };
  
  this.sendForm = function(form$$Node)
  {
    xmlHttp = this.xmlHttp;
    xmlHttp.setRequestHeader("Cache-Control", "no-cache");
    xmlHttp.send(new FormData(form$$Node.$));
  };
  
  this.xmlHttp = this.getXHR();
  if(this.xmlHttp === null)
    return null;
  this.sett(settings);
  return this;
}

function $$Ajax(settings)
{
  this.xmlHttp = null;

  this.getXHR = function()
  {
    xmlHttp = null;
    try
    { // Firefox, Opera 8.0+, Safari
      xmlHttp=new XMLHttpRequest();
    }
    catch (e)
    { // Internet Explorer
      try
      {
        xmlHttp=new ActiveXObject("Msxml2.XMLHTTP");
      }
      catch (e)
      {
        xmlHttp=new ActiveXObject("Microsoft.XMLHTTP");
      }
    }
    if(xmlHttp === null)
      alert("Browser does not support HTTP Request");
    return xmlHttp;
  };

  this.sett = function(s)
  {
    xmlHttp=this.xmlHttp;
    s.success === undefined ? xmlHttp.$$success = function(){} : xmlHttp.$$success = s.success;
    s.error === undefined ? xmlHttp.$$error = function(){} : xmlHttp.$$error = s.error;
    s.complete === undefined ? xmlHttp.$$complete = function(){} : xmlHttp.$$complete = s.complete;

    this.open(s.method, s.url, s.async, s.user, s.password);
  };

  this.open = function(method, url, async, user, password)
  {
    xmlHttp = this.xmlHttp;
    switch(true)
    {
      case method === undefined:
      case url === undefined:
        throw new Error("$$Ajax::method & url args are required");
        break;
      case async === undefined:
        xmlHttp.open(method , url, true);
        break;
      case user === undefined:
        xmlHttp.open(method , url, async);
        break;
      case password === undefined:
        xmlHttp.open(method , url, async, user);
        break;
      default:
        xmlHttp.open(method , url, async, user, password);
    }
    if(method === "POST" || method === "post")
      xmlHttp.setRequestHeader("Content-Type", "application/x-www-form-urlencoded");
    return this;
  };

  this.send = function(data)
  {
    xmlHttp=this.xmlHttp;
    xmlHttp.onreadystatechange=this.onready;
    if(data === undefined)
    {
      if(settings.data === undefined)
        xmlHttp.send();
      else
        xmlHttp.send(settings.data);
    }
    else
      xmlHttp.send(data);
  };

  this.onready = function()
  {
    if(xmlHttp.readyState === 4)
    {
      if(xmlHttp.status === 200)
        xmlHttp.$$success(xmlHttp);
      else
        xmlHttp.$$error(xmlHttp);
      xmlHttp.$$complete(xmlHttp);
    }
  };

  this.xmlHttp = this.getXHR();
  if(this.xmlHttp === null)
    return null;
  this.sett(settings);
  return this;
}

function $$_GetXHR()
{
  var xmlHttp=null;
  try
  { // Firefox, Opera 8.0+, Safari
    xmlHttp=new XMLHttpRequest();
  }
  catch (e)
  { // Internet Explorer
    try
    {
      xmlHttp=new ActiveXObject("Msxml2.XMLHTTP");
    }
    catch (e)
    {
      xmlHttp=new ActiveXObject("Microsoft.XMLHTTP");
    }
  }
  if(xmlHttp === null)
    alert("Browser does not support HTTP Request");
  return xmlHttp;
}

// Popup code

$$Popup = new Object();
$$Popup.popups = null;
$$Popup.addCss = true;
$$Popup.formBorderColor = "#111";
$$Popup.formBackgroundColor = "#bbb";
$$Popup.captionColor = "#fff";
$$Popup.captionHoverColor = "#fff";
$$Popup.captionBorderColor = "#555";
$$Popup.captionBackgroundColor = "#000";
$$Popup.captionBackgroundHoverColor = "#933";
$$Popup.contentColor = "#000";

$$Popup.init = function()
{  
  if($$Popup.popups !== null)
    return;
  $$Popup.popups = [];
  
  if($$Popup.addCss === true)
  {
    var css =
      '.lajaxPopupForm { display:table; position:fixed; left:30%; top:100px; height:400px; width:400px; background-color:'+ $$Popup.formBackgroundColor +'; border:solid 2px; border-color:'+$$Popup.formBorderColor+';z-index = 35000;}'
      + '.lajaxPopupCaption { display:table; height:28px; width:100%; color:'+$$Popup.captionColor+'; background-color:'+ $$Popup.captionBackgroundColor +'; border:solid 0px; border-bottom-width:1px; border-color:'+$$Popup.captionBorderColor+';}'
      + '.lajaxPopupTitle{ display:table-cell; text-align:center; vertical-align:middle;}'
      + '.lajaxPopupOption{ display:table-cell; text-align:center; vertical-align:middle; width:30px; border:solid 0px; border-left-width:1px; border-color:' + $$Popup.captionBorderColor + ';}'
      + '.lajaxPopupOption:hover{ color:'+$$Popup.captionHoverColor+'; background-color:' + $$Popup.captionBackgroundHoverColor + ';}'
      + '.lajaxPopupContentHolder{ display:table; height:calc(100% - 28px); width:100%;}'
      + '.lajaxPopupContent{ display:table-cell; padding:10px; overflow:auto; text-align:center; vertical-align:middle; color:' + $$Popup.contentColor + ';}';

    var style = document.createElement('style');

    if (style.styleSheet)
      style.styleSheet.cssText = css;
    else
      style.appendChild(document.createTextNode(css));
    document.getElementsByTagName('head')[0].appendChild(style);
  }
};

$$Popup.handleDragStart = function(event)
{
  event.dataTransfer.setData("screenX", event.screenX);
  event.dataTransfer.setData("screenY", event.screenY);

  event.target.parentNode.style.opacity = 0.25;
};

$$Popup.handleDragEnd = function(event)
{
  var x = event.dataTransfer.getData("screenX");
  var y = event.dataTransfer.getData("screenY");
  
  var bounds = event.target.getBoundingClientRect();
  
  x = bounds.left + event.screenX - x ;
  y = bounds.top + event.screenY - y;

  event.target.parentNode.style.left = x + "px";
  event.target.parentNode.style.top = y + "px";
  
  event.target.parentNode.style.opacity = "";
};

$$Popup.create = function(caption, content, afterCreateCallback)
{
  $$Popup.init();

  var popupContent = document.createElement("div");
  popupContent.className = "lajaxPopupContent";
  popupContent.innerHTML = content;
  
  var popupContentHolder = document.createElement("div");
  popupContentHolder.className = "lajaxPopupContentHolder";
  popupContentHolder.appendChild(popupContent);
  
  var popupTitle = document.createElement("div");
  popupTitle.className = "lajaxPopupTitle";
  popupTitle.innerHTML = caption;
  
  var popupClose = document.createElement("span");
  popupClose.className = "lajaxPopupOption";
  popupClose.innerHTML = "X";
    
  var popupCaption = document.createElement("div");
  popupCaption.className = "lajaxPopupCaption";
  popupCaption.setAttribute("draggable", "true");
  popupCaption.setAttribute("ondragstart", "$$Popup.handleDragStart(event);");
  popupCaption.setAttribute("ondragend", "$$Popup.handleDragEnd(event);");
  popupCaption.appendChild(popupTitle);
  popupCaption.appendChild(popupClose);
    
  var popupHolder = document.createElement("div");
  popupHolder.className = "lajaxPopupForm";
  popupHolder.appendChild(popupCaption);
  popupHolder.appendChild(popupContentHolder);
  
  popupClose.onclick = function(){$$Popup.close(popupHolder);};

  $$Popup.popups.push(popupHolder);

  document.getElementsByTagName('body')[0].appendChild(popupHolder);
  
  if(afterCreateCallback !== undefined)
    afterCreateCallback();  
  return this;
};

$$Popup.createFromLoad = function(caption, url, method, postData, afterCreateCallback)
{
  if(method === undefined)
    method = "GET";
  
  var ajax;
  if(method === "POST" || method === "post")
    ajax = $$.ajax({method:method, data:postData, url:url, success:function(resp){$$Popup.create(caption, resp.responseText, afterCreateCallback);}});
  else
    ajax = $$.ajax({method:method, url:url, success:function(resp){$$Popup.create(caption, resp.responseText, afterCreateCallback);}});
  ajax.send();
  
  return this;
};

$$Popup.createFromLoadForm = function(caption, form$$Node, url, method, afterCreateCallback)
{
  if(method === undefined)
  {
    if(form$$Node.$.method === undefined)
      method = "POST";
    else
      method = form$$Node.$.method;
  }

  if(url === undefined)
  {
    if(form$$Node.$.action === undefined)
      url = "";
    else
      url = form$$Node.$.action;
  }

  var args = $$.getFormArgs(form$$Node);
  var ajax;
  if(method === "POST" || method === "post")
    ajax = $$.ajax({method:method, data:args, url:url, success:function(resp){$$Popup.create(caption, resp.responseText, afterCreateCallback);}});
  else
    ajax = $$.ajax({method:method, url:url+"?"+args, success:function(resp){$$Popup.create(caption, resp.responseText, afterCreateCallback);}});
  ajax.send();
  
  return this;
};
$$Popup.reloadContent = function(popup, newContent)
{
  $$Popup.init();
  
  for(var i=0; i<$$Popup.popups.length; i++)
  {
    var element = $$Popup.popups[i];
    if(element === popup || element.contains(popup))
    {
      var elems = element.getElementsByClassName('lajaxPopupContent');
      if(elems.length > 0)
        elems[0].innerHTML = newContent;
      return;
    }
  }
};
$$Popup.close = function(popup)
{
  $$Popup.init();
  
  for(var i=0; i<$$Popup.popups.length; i++)
  {
    var element = $$Popup.popups[i];
    if(element === popup || element.contains(popup))
    {
      $$Popup.popups.splice(i, 1);
      element.parentNode.removeChild(element);
      return;
    }
  }
};

String.equalIgnoreCase = function(str1, str2)
{
  return str1 !== null && str2 !== null
    && typeof str1 === 'string' && typeof str2 === 'string'
    && new RegExp("^" + str1.replace(/[-\/\\^$*+?.()|[\]{}]/g, '\\$&') + "$", "i").test(str2);
};

String.prototype.equalIgnoreCase = function(str)
{
  return String.equalIgnoreCase(this, str);
};